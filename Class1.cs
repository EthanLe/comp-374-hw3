﻿using System;

public class Class1
{
	public Class1()
	{
        Class BoundBuffer<T>{
        private Queue<T> _queue = new Queue<T>();
        private Mutex _lock = new Mutex();
        private Semaphore _full = new Semaphore(0,10);
        private Semaphore _empty = new Semaphore(10, 10);

        public void Enqueue(T item) {
               _empty.WaitOne();
            _lock.WaitOne();
            _queue.Enqueue(item);
            _lock.Exit();
            _full.Release(1);
        }

    public T Dequeue(){
        T value;
        _full.WaitOne();
        var taken = false;
        _lock.Enter(ref taken);
        value = _queue.Dequeue();
        _lock.Exit();
        _empty.Release(1);
        return value;
    }
    }

	}
}
