﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace Homework3
{
    class BoundBuffer<T>
    {
        private static int BUFFER_SIZE = 10000; 
        private int count = 0; 
        private int inPos = 0;   
        private int outPos = 0;  
        private Object[] buffer = new Object[BUFFER_SIZE]; 
        
        private SpinLock _lock = new SpinLock();
        private Semaphore empty = new Semaphore(BUFFER_SIZE, BUFFER_SIZE);
        private Semaphore full = new Semaphore(0, BUFFER_SIZE); 

        public void insert(Object item)
        {

            empty.WaitOne();
            var taken = false;
            _lock.Enter(ref taken);
            ++count;
            buffer[inPos] = item;
            inPos = (inPos + 1) % BUFFER_SIZE;
            _lock.Exit();
            full.Release(1);
        }

        public long remove()
        {
            Object item = null;

            full.WaitOne(); 
            var taken = false;
            _lock.Enter(ref taken);
            --count;
            item = buffer[outPos];
            outPos = (outPos + 1) % BUFFER_SIZE;
            _lock.Exit();
            empty.Release(1);
            return (long)item;
        }

        public int Size()
        {
            return buffer.Length;
        }

    }
}